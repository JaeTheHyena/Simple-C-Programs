#include <stdio.h>
#include <math.h>

int main()
{
	/* Declaration des variables */
	int a, b, c;
	
	/* Calcul */
	printf("Saisir a:\n");
	scanf("%d", &a);
	
	printf("Saisir b:\n");
	scanf("%d", &b);
	
	c = a * b;
	
	printf("\naxb=%d\n", c);
	
	/* fin */
	system("PAUSE");
}
