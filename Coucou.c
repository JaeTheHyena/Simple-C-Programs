#include <stdio.h>

int main()
{
	/* Definition des variables */
	char a;
	char b;
	char c;
	char d;
	char e;
	char f;
	
	/* Definition des valeurs */
	a='c';
	b='o';
	c='u';
	d='C';
	e='O';
	f='U';
	
	/* Execution */
	printf("%c %c %c %c %c %c \n", a, b, c, d, e, f);
	
	/* Fin */
	system("PAUSE");
}
