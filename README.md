# Simple-C-Programs  
Juste des petits programmes en C.  
Libre a vous de les reprendre et des les modifier.  
  
Cloner le repo:  
```git
git clone https://github.com/Reotip/Simple-C-Programs.git
```  
Pour les compiler:
  - Linux: [GNU-GCC](https://gcc.gnu.org)  
  - Windows: [Dev-CPP](https://sourceforge.net/projects/orwelldevcpp/)  
  - MAC: Voir la categorie  
  
Compiler les programmes sous Linux:  
```git
git clone https://github.com/Reotip/Simple-C-Programs.git
cd Simple-C-Programs
gcc <nomdufichier>.c -o <nomquevousvoulez>
./<nomquevousavezchoisi>
```  
Certaines portions de code peuvent ne pas etre disponible sous Linux:  
```c
system("PAUSE");
```   
  
Compiler le programme sous MacOS:
  - Ouvrir un invite de commande
  - Executer cette commande
```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
  - Une fois la commande finie, executer
```bash
brew install gcc
```
  - Une fois cette commande terminee, suivre le tuto linux pour compiler les programmes

Repo par [Jae Beojkkoch](https://github.com/JaeTheHyena)
