#include <stdio.h>

int main()
{
	/* Declaration des variables */
	int a, b, c;
	
	/* Calcul */
	printf("Saisir a:\n");
	scanf("%d", &a);
	printf("Saisir b:\n");
	scanf("%d", &b);
	
	c = a-b;
	
	printf("\na-b= %d\n", c);
	
	/* Fin */
	system("PAUSE");
}
