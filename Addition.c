#include <stdio.h>

int main()
{
	/* Declaration des variables */
	int a, b, c;
	
	/* Assignation des valeurs */
	a, b, c = 0;

	
	/* Demande des valeurs */
	printf("Valeur de a:\n");
	scanf("%d", &a);
	
	printf("Valeur de b:\n");
	scanf("%d", &b);
	
	system("PAUSE");
	
		
	/* Calcul */
	c = a+b;
	
	/* Calcul */
	printf("\na+b=%d\n", c);
	
	/* Fin */
	system("PAUSE");
}
