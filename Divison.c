#include <stdio.h>
#include <math.h>

int main()
{
	/* Declaration des variables */
	float a, b, c;
	
	/* Calcul */
	printf("Saisir a:\n");
	scanf("%f", &a);
	
	printf("Saisir b:\n");
	scanf("%f", &b);
	
	c = a / b;
	
	printf("\na/b=%f\n", c);
	
	/* Fin */
	system("pause");
}
